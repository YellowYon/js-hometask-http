'use strict';
async function addUserFetch(userInfo) {
  if (!userInfo.hasOwnProperty('id')) {
    // напишите POST-запрос используя метод fetch
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userInfo),
    };
    const response = await fetch('http://localhost:3000/users', options);
    //ждём пока выполнится запрос и кладём в переменную response

    if (response.ok === true) {
      //ok - код 200-299
      const jsonResponse = await response.json();
      //ждём пока зарезолвится промис и преобразуем ответ в json

      return jsonResponse.id;
    } else {
      throw new Error(`${response.status} ${response.statusText}`);
    }
  } else {
    throw new Error('UserInfo не должен содержать поле id');
  }
}

async function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  if (!isNaN(id)) {
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const f = await fetch(`http://localhost:3000/users/${id} `, options);
    if (f.ok === true) {
      return await f.json();
    } else {
      throw new Error(`${f.status} ${f.statusText}`);
    }
  }
  throw new Error('id не число');
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  if (!userInfo.hasOwnProperty('id')) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function () {
        if (xhr.readyState != 4) {
          return;
        }
        if (xhr.status === 201) {
          resolve(JSON.parse(xhr.responseText).id);
        } else {
          reject(`${xhr.status} ${xhr.statusText}`);
        }
      };

      xhr.open('POST', 'http://localhost:3000/users');
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify(userInfo));
    });
  } else {
    return Promise.reject('UserInfo не должен содержать поле id');
  }
}

function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  if (!isNaN(id)) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onloadend = () => {
        if (xhr.status >= 200 && xhr.status <= 299) {
          resolve(JSON.parse(xhr.responseText));
        } else {
          reject(`${xhr.status} ${xhr.statusText}`);
        }
      };
      xhr.open('GET', `http://localhost:3000/users/${id}`);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send();
    });
  } else {
    return Promise.reject('id не число');
  }
}

function checkWork() {
  addUserXHR({ name: 'Alice', lastname: 'FetchAPI' })
    .then((userId) => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then((userInfo) => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch((error) => {
      console.error(error);
    });
}

checkWork();
